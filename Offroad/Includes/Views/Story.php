<?php
        require_once("DB.php");
        class Story
        {
            //Metode der returnere alle historier fra database
            function returnAllStoriesView()
            {
                $q = "SELECT * FROM story";
                $db = new MySqlDatabase();
                $result = $db->query($q);
                $storyCount = $result->num_rows;
                //Funktion der returnerer alle historier fra arrayet.
                while($row = $result->fetch_assoc()) {
                    $storyCount--;
                    if ($storyCount % 2 == 0)
                    {
                        echo "<li class='timeline-inverted'>";
                    }else {
                        echo "<li>";
                    };
                    echo "<div class='timeline-image'>";
                    echo "<img class='img-circle img-responsive' src='../Resources/Img/" . $row["StoryImg"] . "' alt=''>";
                    echo "</div>";
                    echo "<a href='StoryDetail.php?id=" . $row["StoryID"] . "'>";
                    echo "<div class='timeline-panel'>";
                    echo "<div class='timeline-heading'>";
                    echo "<h4>" . $row["StoryDate"] . "</h4>";
                    echo "<h4 class='subheading'>" . $row["StoryName"] . "</h4>";
                    echo "</div>";
                    echo "<div class='timeline-body'>";
                    echo "<p class='text-muted'>" . $row["StoryDescription"] . "</p>";
                    echo "</div>";
                    echo "</a>";
                    echo "</div>";
                    echo "</li>";
                }
            }
            //Denne metode forespørger på alle historierne i databasen
            function returnStoryDetailView($storyid){ //Parameteren kommer fra StoryDetail-siden
                $q = "SELECT * FROM story WHERE StoryID = $storyid";
                $db = new MySqlDatabase();
                $result = $db->query($q);
                $value = mysqli_fetch_assoc($result); //Omskrive object til assocciativt array, bruges kun, når vi kun vil have 1 række
                echo "<h2>" . $value["StoryName"] . "</h2>";
                echo "<figure>";
                echo "<img class='img-responsive img-centered figure-img img-fluid img-rounded' src='../Resources/Img/" . $value["StoryImg"] . "' alt='' style='width:300px;'>";
                echo "<figcaption class='figure-caption'>" . $value["StoryImgCaption"] . "</figcaption>";
                echo "</figure>";
                echo "<br>";
                echo "<p>" . $value["StoryDescription"] . "</p>";
            }
            
            function PostNewStory($storyname, $storydescription, $storydate, $storyimage) {
                $q = "INSERT INTO story(StoryName, StoryDescription, StoryDate, StoryImg) VALUES('$storyname', '$storydescription', '$storydate', '$storyimage')";
                $db = new MySqlDatabase();
                $db->query($q);

                


            }
           
        }
?>
<?php

/* class Story{

    public $stories = array(
        array(
            "StoryId"=>1,
            "StoryDate"=>"13. marts 2011",
            "StoryName"=>"Opera at the beach",
            "StoryDescription"=>"SUPER begivenhed jeg deltog i!",
            //"Lat"=>"56.4",
            //"Long"=>"9",
            "StoryImage"=>"../Resources/Img/Flame.PNG"
        ),
        array(
            "StoryId"=>2,
            "StoryDate"=>"14. marts 2011",
            "StoryName"=>"Country at the beach",
            "StoryDescription"=>"SUPER begivenhed jeg deltog i!",
            //"Lat"=>"56.4",
            //"Long"=>"9",
            "StoryImage"=>"../Resources/Img/Havet.JPG"
        ),
        array(
            "StoryId"=>3,
            "StoryDate"=>"15. marts 2011",
            "StoryName"=>"HipHop at the beach",
            "StoryDescription"=>"SUPER begivenhed jeg deltog i!",
            //"Lat"=>"56.4",
            //"Long"=>"9",
            "StoryImage"=>"../Resources/Img/Havn.JPG"
        ),
        array(
            "StoryId"=>4,
            "StoryDate"=>"16. marts 2011",
            "StoryName"=>"Metal at the beach",
            "StoryDescription"=>"SUPER begivenhed jeg deltog i!",
            //"Lat"=>"56.4",
            //"Long"=>"9",
            "StoryImage"=>"../Resources/Img/Land.JPG"
        ),
        array(
            "StoryId"=>5,
            "StoryDate"=>"17. marts 2011",
            "StoryName"=>"Rock at the beach",
            "StoryDescription"=>"SUPER begivenhed jeg deltog i!",
            //"Lat"=>"56.4",
            //"Long"=>"9",
            "StoryImage"=>"../Resources/Img/Vinter.JPG"
        )
    ); */

   /*

    //En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
    $sql = "SELECT * FROM story";

    //Resultatet fra forespørgslen gemmes i resultat variablen
    $result = $conn->query($sql);

    function return_all_stories(){ //funktion der returnerer alle historier
        $container = '';    //variable til at lægge data ind i og senere returnerer uden for functionen
        
            foreach($this->stories as $story){  //foreach løkke der kigger på alle arrays i array stories
                $even_number = $story["StoryId"]; //variable til at indeholde StoryId-tallet
                if(is_array($story)){   //Spørger om man kigger på et array i et array
                    if($even_number % 2 == 0){  //Hvis StoryId % 2 giver et helt tal, gør da:
                        $container .=
                        '<li class="timeline-inverted">'.
                            '<div class="timeline-image">'.
                                '<img class="img-circle img-responsive" src="' . $story["StoryImage"] . '" alt="">'.
                            '</div>'.
                            '<div class="timeline-panel">'.
                                '<div class="timeline-heading">'.
                                    '<h4>'.$story["StoryDate"].'</h4>'.
                                    '<h4 class="subheading">'.$story["StoryName"].'</h4>'.
                                '</div>'.
                                '<div class="timeline-body">'.
                                    '<p class="text-muted">'.$story["StoryDescription"].'</p>'.
                                '</div>'.
                            '</div>'.
                        '</li>';
                    }else{ //Hvis StoryId % 2 ikke giver et helt tal, gør da:
                        $container .=
                        '<li>'.
                            '<div class="timeline-image">'.
                                '<img class="img-circle img-responsive" src="' . $story["StoryImage"] . '" alt="">'.
                            '</div>'.
                            '<div class="timeline-panel">'.
                                '<div class="timeline-heading">'.
                                    '<h4>'.$story["StoryDate"].'</h4>'.
                                    '<h4 class="subheading">'.$story["StoryName"].'</h4>'.
                                '</div>'.
                                '<div class="timeline-body">'.
                                    '<p class="text-muted">'.$story["StoryDescription"].'</p>'.
                                '</div>'.
                            '</div>'.
                        '</li>';
                    }
                    
                }else{ //Hvis ikke array i array
                    echo "out";
                }
            }
             return $container; //Returnerer data i variablen $container
            //Denne funktion skal returnere alle event i events arrayet
        }
        
    }

     //Lukker forbindelsen
    $conn->close();
 */
?>