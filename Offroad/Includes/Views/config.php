<?php 
    //Lokale variabler til brug længere nede
    //Variablerne bruges til forbindelsen DB serveren
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "educationdb";

    defined('DB_SERVER') ? null : define('DB_SERVER', $servername);
    defined('DB_NAME') ? null : define('DB_NAME', $dbname);
    defined('DB_USER') ? null : define('DB_USER', $username);
    defined('DB_PASS') ? null : define('DB_PASS', $password);
?>