<!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Alle kan bidrage med historier fra Off Road 2017</div>
                <div class="intro-heading">Historier fra Off Road 2017!</div>
                <a href="#about" class="page-scroll btn btn-xl">Historier</a>
                <a href="#contact" class="page-scroll btn btn-xl">Opret Historie</a>
            </div>
        </div>
    </header>
