<?php
    //Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //Tekststreng (string)
    $firstname = "Casper";

    //Tekststreng (string)
    $middelname = "Wirenfeldt";

    //Tekststreng (string)
    $lastname = "Hald";

    //Nummerisk heltal (integer)
    $age = 20;

    //Boolean (sandt/falsk)
    $inRelationship = false;

    //Tekststreng
    $work = "Kontrollør";

    //Tekststreng
    $workPlace = "Kulturcenteret Limfjorden";

    //Array (en række)
    $hobbies = ["computer","at tegne","motion","bagning","natur"];

?>