<?php
    /* Datatyper
     * Boolean
     * Denne datatype definerer en variable som sand eller falsk
    */

    //Variablen $a har en sand værdi
    $a = true;
    echo $a;
    var_dump($a);
    
    //Variablen har en falsk værdi
    $a = false;
    echo $a;
    var_dump($a);

    //Variablen har en sand værdi
    $a = 5;

    if($a == true)
    {
        var_dump($a);
        echo 'a$ har en sand værdi';
    }

    //Nu sætter vi $a til at være en tekststreng uden nogen værdi.
    $a = "";
    if($a == false)
    {
        var_dump($a);
        echo '<br>';
        echo 'a$ er nu false';
    }

?>