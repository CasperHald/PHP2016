<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                <?php
                    include("Data.php");
                    echo $First_name . "</br>";
                    echo $Last_name . "</br>";
                    echo $email . "</br>";
                    echo $phone . "</br>";
                ?>
            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>
                <?php
                    foreach($preferences as $Interest){
                        echo $Interest . "</br></br>";
                    }
                ?>
            </div>
            <div class="col-md-3">
                <h2>Portefølje!</h2>
                <?php
                    foreach($jobs as $Job => $Work){
                        echo "Hos " . $Job . " Stod jeg for at styre deres " . $Work . "." . "</br></br>";
                    }
                ?>
            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>
                <?php
                    // echo $competencies[0] . " i:" .  "</br>";
                    // echo $competencies[1][0] . " " . $competencies[1][1] . " " . $competencies[1][2] . "</br>";
                    // echo $competencies[2][0] . " " . $competencies[2][1] . " " . $competencies[2][2] . "</br>";

                     foreach ($competencies as $comp) { //Foreach løkke henter variablen $competencies - giver navnet $comp
                            if(is_array($comp)){        //if - hvis et array indeholder et array gør således
                                foreach ($comp as $c) {  //Foreach løkke - $comp som $c
                                    echo "<li>" . $c . "</li>"; //Echo/udskriv lister med $c
                                } //Slut foreach
                            }else {                              //Else - Ellers gør
                                echo "<li>" . $comp . "</li>";   //Echo/Udskriv lister med $comp
                            } //Slut else
                        } //Slut Overordnet foreach
                ?>
            </div>
        </div>
    </div>
<body>