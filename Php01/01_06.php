<?php
    // Datatyper
    // - Integer
    // - Float


    $a = 1234; // Decimal number 
    var_dump($a);

    $a = -123; // a Negative number 
    var_dump($a);

    $a = 0123; // octal number (equivalent to 83 decimal)
    var_dump($a);

    $a = 1.3;
    var_dump($a);

    $large_number = 2147483647;
    var_dump($large_number);       //int(2147483647)

    $large_number = 2147483648;
    var_dump($large_number);       ///Float(2147483647)


    $milion = 1000000;
    $large_number = 50000 * $milion;
    var_dump($large_number);        //Float(50000000000)

    var_dump(25/7);                 //Float(3.5714285714286)

    var_dump((int) (25/7));         //Int(3)

    var_dump(round(25/7));          //Float(4)



?>