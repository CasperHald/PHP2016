<?php
    //Datatyper
    //String eller tekststrengværdier


    $a = "Goddag! Dette er en tekststreng </br>";
    echo $a;

    $a = 'Dette er sgu også en tekststreng </br>';
    echo $a;


    $name = "Casper";
    echo "Hej, $name";
    echo "</br>";
    echo "Hej igen, $name";

    $a = "<p> Dette er en teksttreng, der indeholder afsnitselementer </p>";
    echo $a;

    date_default_timezone_set("Europe/Copenhagen");
    $a = "<div> Dato i dag er: " . date("d. m. Y") . " og kl. er: " . date("h:i:s");
    echo $a;

    //Teste om  to strenge er ens 
    echo "</br>";
    echo "</br>";
    echo "Er de to variabler ens </br>";
    $a = "abc";
    $b = "abc";

    if($a == $b){
        echo 'Ja, $a og $b er ens </br>';
    }

?>