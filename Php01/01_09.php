<?php
    /* Datatyper
     * Array
     * En datatype og kan betragtes som en samling / collection. En agregering af atome elementer
     * Elementerne i et array kan identificeres ved deres position. Læg mærke til, at en array starte i position 0.
     * Elementer kan også identificeres ved et navn. Disse kaldes på associative arrays.
     */
    
    
    $person[0] = "Edison";
    $person[1] = "Wankel";
    $person[2] = "Crapper";    

    echo $person[0];
    echo "</br>";
    echo $person[1];
    echo "</br>";
    echo $person[2];
    echo "</br>";
    echo "</br>";

    $creator['Pæren'] = "Edison";
    $creator['Motoren'] = "Wankel";
    $creator['Toilet'] = "Crapper";

    echo $creator['Toilet'];

    echo "</br>";

    echo $creator['Pæren'];

    echo "</br>";

    echo $creator['Motoren'];

    echo "</br>";
    echo "</br>";

    //Dette array kan referes til ved variablen $person. Elementerne kan identificeres ved position
    $person = array("Edison", "Wankel", "Crapper");
    
    //Dette array kan referes til ved variablen $creator. I et associativt array indgå en nøgle og en værdi.
    $creator = array('Pæren' => "Edison", 'Motoren' => "Wankel", 'Toiletet' => "Crapper");

    //Vi kan løbe en array igennem og udskrive indholdet.

    foreach ($person as $name) {
        echo "Hello, {$name} </br>";
    }

    echo "</br>";

    foreach ($creator as $invention => $inventor) {
        echo $inventor . " opfandt " . $invention . "</br>";
    }
?>