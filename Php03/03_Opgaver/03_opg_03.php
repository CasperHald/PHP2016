<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden getAllValuesFromAnArray skal kunne finde værdierne i et associativt array.
     * Brug den indbyggede metode array_keys().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Keys and values
     */
    
    class Event
    {
        function getAllValuesFromAnArray()
        {   
            $container = ''; //Beholder til vores data
            $event = array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
            );

            var_dump(array_keys($event));
            var_dump(array_values($event));

            $values = array_values($event);
            list($value1, $value2, $value3, $value4, $value5, $value6, $value7) = $values;

            /*foreach (array_keys($event) as $values){
                echo "<li>" . $values . "</li>";
            } */

            //return $value1 . " </br> " . $value2 . " </br> " . $value3 . " </br> " . $value4 . " </br> " . $value5 . " </br> " . $value6 . " </br> " . $value7;

            foreach ($event as $key => $value){ //For hver gang der kommer en key og value i $event - skal løkke gå en omgang
                $container .= "<li>" . $key . " = " . $value . "</li>"; //Keys og values tilføjes til den tomme container
            }

            return $container; //Retunerer den fulde container til objektet
            

        }
    }
    $event = new Event;
    echo $event->getAllValuesFromAnArray();
?>