<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden removePersonFromArray skal kunne fjerne en person fra et indexeret array.
     * Brug den indbyggede metode array_slice().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Slicing Array
     */
    
    class Person
    {
        function removePersonFromArray()
        {
            $people = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
            $newPeopleArray = array_slice($people, 1, 4); //Array-slice sætter en grænselængde på et eksisterende array - det kan kun bruges til at fjerne elementer fra start eller slutningen af arrayet - array_slice skal skabes til en ny variable, får man gang bruge funktioner som list på det.
            //unset($newPeopleArray[2]); - en metode til at ramme et specifikt element
            var_dump($newPeopleArray);
            list($person1, $person2, $person3, $person4) = $newPeopleArray; //Listen skaber variabler på det nye array skabt af array_slice
            return $person1 . " " . $person2 . " " . $person3 . " " . $person4;
        }
    }
    $person = new Person;
    echo $person->removePersonFromArray();
?>