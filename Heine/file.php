  <div class="cg-shopping-toolbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6 visible-lg">
                                            </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 top-bar-right wpml">
                                            </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 visible-md visible-sm visible-xs mobile-search">
                                            </div>
                </div>
            </div>
        </div>

        <div id="wrapper">

        <div class="row top-info-bar">
            <div class="container">
                <div class="col-md-3">
					<div class="topbar1">
					<p><img src="https://streety.dk/wp-content/uploads/2016/06/ane.png"><b>5000+ GLADE KUNDER</b></p>
					</div>
				</div>
                <div class="col-md-3">
					<div class="topbar2">
                    <p><img src="https://streety.dk/wp-content/uploads/2016/06/ane.png"><b>1-3 DAGES LEVERING</b></p>
					</div>
			   </div>
                <div class="col-md-3">
					<div class="topbar3">
                    <p><img src="https://streety.dk/wp-content/uploads/2016/06/ane.png"><b>100 DAGES RETURRET</b></p>
					</div>
				</div>
                <div class="col-md-3">
					<div class="topbar4">
                    <p><img src="https://streety.dk/wp-content/uploads/2016/06/ane.png"><b>GRATIS OMBYTNING</b></p>
					</div>
				</div>
            </div>
        </div>

                            <!-- Default Logo to the left with menu below -->
<div class="cg-menu-below">
    <div class="container">
        <div class="cg-logo-cart-wrap">
            <div class="cg-logo-inner-cart-wrap">
                <div class="row">
                    <div class="container">
                        <div class="cg-wp-menu-wrapper">
                                <div id="load-mobile-menu">
                                </div>
                            
                            <div class="cg-header-search visible-lg">
                                <div id="woocommerce_product_search-3" class="woocommerce widget_product_search">
<form role="search" method="get" class="woocommerce-product-search" action="http://streety.dk/">
	<label class="screen-reader-text" for="woocommerce-product-search-field">Søg efter:</label>
	<input type="search" id="woocommerce-product-search-field" class="search-field" placeholder="Hvad leder du efter?" value="" name="s" title="Søg efter:" />
	<input type="submit" value="Søg" />
	<input type="hidden" name="post_type" value="product" />
</form>
</div>                                                            </div>
                                
                                <div class="logo image">
                                    <a href="http://streety.dk/" rel="home">
                                        <span class="helper"></span><img src="http://streety.dk/wp-content/uploads/2016/06/streety-logo-v2-e1466889185106.png" style="max-width: 300px;" alt="Streety.dk"/></a>
                                </div>
                            </div>
                    </div><!--/container -->
                </div><!--/row -->
            </div><!--/cg-logo-inner-cart-wrap -->
        </div><!--/cg-logo-cart-wrap -->
    </div><!--/container -->
</div><!--/cg-menu-below -->

<div class="cg-primary-menu cg-wp-menu-wrapper cg-primary-menu-below-wrapper">
    <div class="container">
        <div class="row">
            <div class="container">
    <div class="cg-main-menu"><ul id="menu-primary-menu" class="menu"><li id="menu-item-72865" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-72865"><a href="#">OVERDELE</a>
<div class=cg-submenu-ddown><div class='container'>
<div class="cg-menu-img"><span class="cg-menu-title-wrap"><span class="cg-menu-title">OVERDELE</span></span></div><ul class="cg-menu-ul">
	<li id="menu-item-72732" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72732"><a href="http://streety.dk/produkt-kategori/t-shirts/">T-SHIRTS</a></li>
	<li id="menu-item-72735" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72735"><a href="http://streety.dk/produkt-kategori/troejer/">TRØJER</a></li>
	<li id="menu-item-72733" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72733"><a href="http://streety.dk/produkt-kategori/skjorter/">SKJORTER</a></li>
	<li id="menu-item-72734" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72734"><a href="http://streety.dk/produkt-kategori/jakker/">JAKKER</a></li>
</ul></div></div>
</li>
<li id="menu-item-72866" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item menu-item-72866"><a href="#">UNDERDELE</a>
<div class=cg-submenu-ddown><div class='container'>
<div class="cg-menu-img"><span class="cg-menu-title-wrap"><span class="cg-menu-title">UNDERDELE</span></span></div><ul class="cg-menu-ul">
	<li id="menu-item-72867" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72867"><a href="http://streety.dk/produkt-kategori/jeans/">JEANS</a></li>
	<li id="menu-item-73461" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73461"><a href="http://streety.dk/produkt-kategori/joggingbukser/">JOGGINGBUKSER</a></li>
	<li id="menu-item-73504" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73504"><a href="http://streety.dk/produkt-kategori/shorts/">SHORTS</a></li>
	<li id="menu-item-72736" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72736"><a href="http://streety.dk/produkt-kategori/underbukser/">UNDERBUKSER</a></li>
</ul></div></div>
</li>
<li id="menu-item-73358" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-parent-item menu-item-73358"><a href="http://streety.dk/produkt-kategori/tilbehoer/">TILBEHØR</a>
<div class=cg-submenu-ddown><div class='container'>
<div class="cg-menu-img"><span class="cg-menu-title-wrap"><span class="cg-menu-title">TILBEHØR</span></span></div><ul class="cg-menu-ul">
	<li id="menu-item-73359" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73359"><a href="http://streety.dk/produkt-kategori/tasker/">TASKER</a></li>
	<li id="menu-item-73088" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73088"><a href="http://streety.dk/produkt-kategori/caps/">CAPS &#038; HATTE</a></li>
</ul></div></div>
</li>
<li id="menu-item-72737" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-72737"><a href="http://streety.dk/produkt-kategori/sko/">SKO</a></li>
<li id="menu-item-73749" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73749"><a href="http://streety.dk/produkt-kategori/tilbud/">TILBUD</a></li>
<li id="menu-item-73696" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-73696"><a href="http://streety.dk/produkt-kategori/pakketilbud/">PAKKETILBUD</a></li>
</ul></div>                                                                            <div class="cart-wrap">
                                                        
    <ul class="tiny-cart">
        <li>
            <a class="cart_dropdown_link cart-parent" href="http://streety.dk/kurv/" title="Se din kurv">
                <div class="cg-header-cart-icon-wrap">
                     
                        <div class="icon cg-icon-shopping-1"></div>
                                <span class="cg-cart-count">0</span>
                </div>
                <span class='cart_subtotal'><span class="woocommerce-Price-amount amount">0&nbsp;<span class="woocommerce-Price-currencySymbol">DKK</span></span></span>
            </a>
            <ul class="cart_list"><li class="empty">Ingen produkter i kurven.</li></ul>    </li>
    </ul>
                                                        </div>
                                                        </div>
        </div>
    </div>
</div>
