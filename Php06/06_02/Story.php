<?php

    $storyname = $_GET["storyname"];
    $storydescription = $_GET["storydescription"];

    //Lokale variabler til brug længere nede
    //Variablerne bruges til forbindelsen DB serveren
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "educationdb";

    //Opret forbindelse
    $conn = new mysqli($servername, $username, $password, $dbname);
    //Tjek om der er forbindelse
    if($conn->connect_error) { //connect-error fortæller hvad fejlen er - er en metode
        die("Forbindelsen fejlede: " . $conn->connect_error);
    }

    //En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
    if(empty($storydescription)){
        $sql = "SELECT * FROM story WHERE StoryName = '$storyname'";
    }else {
         $sql = "SELECT * FROM story WHERE StoryName = '$storyname'OR StoryDescription Like '%$storydescription%'";
    }
   

    //Resultatet fra forespørgslen gemmes i resultat variablen
    $result = $conn->query($sql);

    //Udskriver data (hvis der er noget i $result)
    if($result->num_rows > 0) {
        //Output data fra hver række
        while($row = $result->fetch_assoc()){
            echo "Navn: " . $row["StoryName"] . " - Beskrivelse: " . $row["StoryDescription"] . " - Dato: " . $row["StoryDate"] . " <img src='Img/" . $row["StoryImg"] . "' style='width:25%;'>" . "</br>";
        }
    } else {
        echo "Ingen resultater fra databasen";
    }

    //Lukker forbindelsen
    $conn->close();

?>